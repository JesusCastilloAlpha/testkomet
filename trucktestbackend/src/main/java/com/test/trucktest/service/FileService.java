package com.test.trucktest.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.test.trucktest.dto.LoadedFile;
import com.test.trucktest.dto.LoadedFileStatus;


/**
 * This class manage file import process.
 * @author jcastillo
 *
 */
@Service
public class FileService {

	
	private Logger log = LoggerFactory.getLogger(FileService.class);
	//@Value("${truck.test.file.dir}")
	@Value("${truck.test.file.dir}")
	String filesDir;
	/**File name for store in disk*/
	String fileImportedName = "import";
	/**File extension for uploaded file*/
	String fileImportedExt="dat";
	
	@Autowired
	TruckService truckService;
	
	
	
	@PersistenceContext
	EntityManager em;
	

	private static String SELECT = "SELECT file_load_id, filename, systemfilename, status, lastprocessedrow, errorqty, loadedqty FROM flieload"; 
	
	private static String SELECT_BY_ID = SELECT + " WHERE file_load_id = ?"; 
	
	private static String SELECT_BY_SYSTEM_FILE_NAME = SELECT + " WHERE systemfilename like ?"; 
	
	private static String SELECT_BY_STATUS = SELECT + " WHERE status like ?"; 
	
	private static String UPDATE = "UPDATE flieload SET filename=?, systemfilename=?, status=?, lastprocessedrow=?, errorqty=?, loadedqty=? "
			+ " WHERE file_load_id=?";
	
	private static String INSERT = "INSERT INTO flieload (file_load_id, filename, systemfilename, status, lastprocessedrow, errorqty, loadedqty)" + 
			" VALUES (?,?,?,?,?,?,?)";
	
	/**
	 * Store file in disk no process data
	 * @param in
	 * @param originalName
	 * @return
	 * @throws IOException
	 */
	@Transactional
	public String storeFile(InputStream in,String originalName) throws IOException {
			
		
		
		File targetFile = getTargetFile(); 
				
		
		//Store first file in disk;	
		
	    java.nio.file.Files.copy(
	      in, 
	      targetFile.toPath(), 
	      StandardCopyOption.REPLACE_EXISTING);
	 
	    IOUtils.closeQuietly(in); 
		
		LoadedFile ld = new LoadedFile();
		
		ld.setSystemFileName(targetFile.getAbsolutePath());
		ld.setStatus(LoadedFileStatus.PROCESSING);
		ld.setFileName(originalName);
		save(ld);	
		
		return null;
	}
	
	
	
	
	@Transactional
	public List<LoadedFile> getAll() {
		return get(SELECT);		
	}
	
	
	@Transactional
	public List<LoadedFile> getByFileSystemName(String fileName) {
		return get(SELECT_BY_SYSTEM_FILE_NAME,fileName);		
	}
	
	@Transactional
	public List<LoadedFile> getByStatus(String status) {
		return get(SELECT_BY_STATUS,status);		
	}
	
	
	@Transactional
	public List<LoadedFile> getByID(Integer id) {
		return get(SELECT_BY_ID,id);		
	}
	
	/**
	 * get LoadedFile by query and parameters
	 * @param query
	 * @param params
	 * @return
	 */
	private List<LoadedFile> get(String query, Object...params){
		Query query0 = em.createNativeQuery(query);
		
		
		for (int i = 1; i<=params.length; i++) {
			query0.setParameter(i, params[i-1]);
		}
		
		@SuppressWarnings("unchecked")
		List<Object[]> res = query0.getResultList();
		
		
		ArrayList<LoadedFile> files = new ArrayList<LoadedFile>();
		
		for (Object[] row : res) {
			files.add(new LoadedFile(
					((BigInteger)row[0]).intValue(),
					(String)row[1],
					(String)row[2],
					LoadedFileStatus.getByCode((String)row[3]),
					((BigInteger)row[4]).intValue(),
					((BigInteger)row[5]).intValue(),
					((BigInteger)row[6]).intValue()					
					));
		}
		
		return files;
	}
	
	

	
	@Transactional
	public LoadedFile save(LoadedFile ld) {
		//(file_load_id, filename, systemfilename, status, lastprocessedrow, errorqty, loadedqty)
		
		//select max(file_load_id) + 1 from flieload
		Query query;

		int i = 1;
		
		boolean isUpdate = false;
		
		
		if(ld.getLoadedFileID()>0) {
			query = em.createNativeQuery(UPDATE);
			isUpdate = true;
		}else {//get next id;
			BigInteger nextID = BigInteger.ONE;
			query = em.createNativeQuery("select coalesce(max(file_load_id),0) + 1 from flieload");
			List<BigInteger> res = query.getResultList();
			if (res.size()>0) {
				nextID =  (BigInteger)(res.get(0));
			}
			
			query = em.createNativeQuery(INSERT);
			query.setParameter(i++, nextID);
			ld.setLoadedFileID(nextID.intValue());
		}
			
		
		query.setParameter(i++, ld.getFileName());
		query.setParameter(i++, ld.getSystemFileName());
		query.setParameter(i++, ld.getStatus().getCode());
		query.setParameter(i++, ld.getLastProcessedRow());
		query.setParameter(i++, ld.getErrorQty());
		query.setParameter(i++, ld.getLoadedQty());		
		if(isUpdate) {//for update
			query.setParameter(i++,ld.getLoadedFileID());
		}
		query.executeUpdate();
		log.debug("Saving " + ld);
		return ld;
	}
	
	/**
	 * Compute next file name
	 * @return
	 */
	private File getTargetFile() {
		File folder = new File(filesDir);
		if (!folder.exists()) {
			folder.mkdir();
			log.debug("Folder created: " + folder.getAbsolutePath());
		}
		
		
		
		File targetFile = new File(filesDir + File.separator + fileImportedName + ".0." + fileImportedExt);		
		int n = 1;
		//Manage version of files
    	while (targetFile.exists()) {
    		targetFile = new File(filesDir + File.separator + fileImportedName + "." +  n + "." + fileImportedExt);
    		n = n+1;
    	}	
    	return targetFile;
	}
	
	
}
