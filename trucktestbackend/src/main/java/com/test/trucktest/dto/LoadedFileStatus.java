package com.test.trucktest.dto;

import com.fasterxml.jackson.annotation.JsonValue;

public enum LoadedFileStatus {
	PROCESSING("Procesando","P"),
	WAITNOTIFY("Por notificar","W"),
	OK("Cargado y Notificado","OK"),
	UNDEFINIED("Indefinido","UN");
	private String name;
	private String code;
	private LoadedFileStatus(String name, String code) {
		this.name = name;
		this.code = code;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}
	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}
	
	public static LoadedFileStatus getByCode(String code) {
		for (LoadedFileStatus s : values()) {
			if (s.code.equalsIgnoreCase(code)) {
				return s;
			}
		}
		return UNDEFINIED;
	}
	
	@JsonValue
	@Override
	public String toString() {
		return name;
	}

}
