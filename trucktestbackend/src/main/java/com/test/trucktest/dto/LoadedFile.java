package com.test.trucktest.dto;


/**
 * File manage
 * @author jcastillo
 *
 */
public class LoadedFile {	
	
	/** File id **/
	private long LoadedFileID;	
	
	/**file name loaded*/
	private String fileName;
	
	/** File name in system*/
	private String systemFileName;
	
	/**Status*/
	private LoadedFileStatus status;
	
	/**Last processed row*/
	private long lastProcessedRow;
	
	/** Quantity error*/
	private long errorQty;
	
	/**Loaded rows quantity*/
	private long loadedQty;
	
	public LoadedFile() {
		super();
	}
	

	public LoadedFile(long loadedFileID, String fileName, String systemFileName, LoadedFileStatus status, long lastProcessedRow,
			long errorQty, long loadedQty) {
		super();
		LoadedFileID = loadedFileID;
		this.fileName = fileName;
		this.systemFileName = systemFileName;
		this.status = status;
		this.lastProcessedRow = lastProcessedRow;
		this.errorQty = errorQty;
		this.loadedQty = loadedQty;
	}

	public long getLoadedFileID() {
		return LoadedFileID;
	}

	public void setLoadedFileID(long loadedFileID) {
		LoadedFileID = loadedFileID;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSystemFileName() {
		return systemFileName;
	}

	public void setSystemFileName(String systemFileName) {
		this.systemFileName = systemFileName;
	}

	public LoadedFileStatus getStatus() {
		return status;
	}

	public void setStatus(LoadedFileStatus status) {
		this.status = status;
	}

	public long getLastProcessedRow() {
		return lastProcessedRow;
	}

	public void setLastProcessedRow(long lastProcessedRow) {
		this.lastProcessedRow = lastProcessedRow;
	}

	public long getErrorQty() {
		return errorQty;
	}

	public void setErrorQty(long errorQty) {
		this.errorQty = errorQty;
	}

	public long getLoadedQty() {
		return loadedQty;
	}

	public void setLoadedQty(long loadedQty) {
		this.loadedQty = loadedQty;
	}
	
	
	

}
