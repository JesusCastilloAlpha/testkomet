package com.test.trucktest.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * Truck model
 * @author jcastillo
 *
 */
public class Truck {
	
	/** id truck */
	private Long truckID;
	
	/** Serial */
	private String serial;
	
	/**Plate*/
	private String plate;
	
	/**Purchase date*/
	private Timestamp purchaseDate;
	
	/**Max weight*/
	private BigDecimal maxWeight;

	/**
	 * @return the truckID
	 */
	public Long getTruckID() {
		return truckID;
	}

	/**
	 * @param truckID the truckID to set
	 */
	public void setTruckID(Long truckID) {
		this.truckID = truckID;
	}

	/**
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * @param serial the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	/**
	 * @return the plate
	 */
	public String getPlate() {
		return plate;
	}

	/**
	 * @param plate the plate to set
	 */
	public void setPlate(String plate) {
		this.plate = plate;
	}

	/**
	 * @return the purchaseDate
	 */
	public Timestamp getPurchaseDate() {
		return purchaseDate;
	}

	/**
	 * @param purchaseDate the purchaseDate to set
	 */
	public void setPurchaseDate(Timestamp purchaseDate) {
		this.purchaseDate = purchaseDate;
	}

	/**
	 * @return the maxWeight
	 */
	public BigDecimal getMaxWeight() {
		return maxWeight;
	}

	/**
	 * @param maxWeight the maxWeight to set
	 */
	public void setMaxWeight(BigDecimal maxWeight) {
		this.maxWeight = maxWeight;
	}
	
	
	

}
